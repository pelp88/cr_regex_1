package main.ru.coderiders.regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class LabWork5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        System.out.println("Have punct. marks? - " +
                Pattern.compile("\\p{Punct}").matcher(input).find());
        System.out.println("Palindrome? - " + Stuff.checkPalindromeByDivision(input));

        String[] out = input.split("\s|[\\p{Punct}]");
        for (String str : out){
            System.out.println(str);
        }

    }
}
