package main.ru.coderiders.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Stuff {
    public static boolean checkPalindromeByDivision(String str){
        int mid = str.length() / 2;
        String[] parts = {str.substring(0, mid),
                str.substring(str.length() % 2 == 0 ? mid : mid + 1)};
        return parts[0].equals(parts[1]);
    }

    public static void match(String regex, String str){
        Matcher matcher = Pattern.compile(regex).matcher(str);

        List<String> matches = new ArrayList<>();
        while (matcher.find()) {
            matches.add(matcher.group(0));
        }

        System.out.println(String.format("Original string - %s\n", str)
                + "Result - " + matches);
    }

}
