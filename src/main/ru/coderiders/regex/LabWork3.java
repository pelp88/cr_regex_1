package main.ru.coderiders.regex;

public class LabWork3 {
    public static void main(String[] args) {
        String regexp = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

        String test = "255.255.0.0";

        Stuff.match(regexp, test);
    }
}
