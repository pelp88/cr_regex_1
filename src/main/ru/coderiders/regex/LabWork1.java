package main.ru.coderiders.regex;

public class LabWork1 {
    public static void main(String[] args) {
        String regexp = "[-]?[0-9]+[.,][0-9]+([e][+][0-9]+)?";

        String test = "1.213 23.12e+10 1,10 123 23,12e+2312";

        Stuff.match(regexp, test);
    }
}
